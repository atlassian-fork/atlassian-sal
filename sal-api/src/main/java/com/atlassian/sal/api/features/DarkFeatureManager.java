package com.atlassian.sal.api.features;

import com.atlassian.sal.api.user.UserKey;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Optional;

/**
 * <p>Provides a cross-product method for determining whether a dark feature is enabled.</p>
 *
 * <p>Implementing products can back these checks with their own internal dark feature management system, but must
 * follow the enable and disable dark features on startup based on system properties and the contents of an optional
 * properties file. To avoid clashes with other system properties all features specified as system property must be
 * prefixed with {@link DarkFeatureManager#ATLASSIAN_DARKFEATURE_PREFIX}. The prefix is removed from the feature key
 * when it is processed later. Values must be either true or false. The location of the dark features property file can
 * be overridden with the {@link DarkFeatureManager#DARKFEATURES_PROPERTIES_FILE_PROPERTY} system property.</p>
 *
 * <p>See SystemDarkFeatureInitializer in sal-core for an implementation.</p>
 *
 * @since 2.10
 */
public interface DarkFeatureManager {
    /**
     * Prefix for all dark feature specified as system property.
     */
    String ATLASSIAN_DARKFEATURE_PREFIX = "atlassian.darkfeature.";

    /**
     * System property for disabling all dark features.
     */
    String DISABLE_ALL_DARKFEATURES_PROPERTY = "atlassian.darkfeature.disabled";

    /**
     * System property for overriding location of dark features property file.
     */
    String DARKFEATURES_PROPERTIES_FILE_PROPERTY = "darkfeatures.properties.file";

    /**
     * Default properties file name.
     */
    String DARKFEATURES_PROPERTIES_FILE_PROPERTY_DEFAULT = "atlassian-darkfeatures.properties";

    /**
     * Checks whether the product contains the given feature for all users, regardless whether the feature can be
     * changed during runtime or not.
     *
     * @param featureKey key of the feature to be checked
     * @return Optional boolean, <code>true</code> if the feature key is valid and enabled, false otherwise;
     * Optional.empty() when the featureKey is not know or is invalid as per
     * {@link ValidFeatureKeyPredicate#isValidFeatureKey(String)}
     * @see ValidFeatureKeyPredicate
     */
    @Nonnull
    Optional<Boolean> isEnabledForAllUsers(@Nonnull String featureKey);

    /**
     * Checks whether the product contains the given feature for all users or for the current user only
     * (must be called within the context of a request). If the user couldn't be resolved or is anonymous,
     * only features enabled for all users are considered.
     *
     * @param featureKey key of the feature to be checked
     * @return Optional boolean, <code>true</code> if the feature is valid and enabled, either for all users or the
     * current user only; <code>false</code> otherwise. Optional.empty() when the featureKey is not know or is invalid
     * as per {@link ValidFeatureKeyPredicate#isValidFeatureKey(String)}
     * @see ValidFeatureKeyPredicate
     */
    @Nonnull
    Optional<Boolean> isEnabledForCurrentUser(@Nonnull String featureKey);

    /**
     * Checks whether the product contains the given feature for all users or just for the given user. In case the user
     * is anonymous, only features enabled for all users are considered.
     *
     * @param userKey    the key of the user being queried; <code>null</code> represents the anonymous user
     * @param featureKey key of the feature to be checked
     * @return Optional boolean, <code>true</code> if the feature key is valid and enabled, either for all users or the
     * current user only; <code>false</code> otherwise; Optional.empty() when the featureKey is not know or is invalid
     * as per {@link ValidFeatureKeyPredicate#isValidFeatureKey(String)}
     * @throws IllegalArgumentException if the user doesn't exist
     * @see ValidFeatureKeyPredicate
     */
    @Nonnull
    Optional<Boolean> isEnabledForUser(@Nullable UserKey userKey, @Nonnull String featureKey);

    /**
     * Checks if a dark feature is enabled for all users, regardless whether the feature can be changed during runtime
     * or not.
     *
     * @param featureKey key of the feature to be checked
     * @return <code>true</code> if the feature key is valid and enabled, false otherwise
     * @see ValidFeatureKeyPredicate
     *
     * @deprecated use {@link #isEnabledForAllUsers(String)} instead
     */
    @Deprecated
    boolean isFeatureEnabledForAllUsers(String featureKey);

    /**
     * Checks if a dark feature is enabled for all users or for the current user only (must be called within the context
     * of a request). If the user couldn't be resolved or is anonymous, only features enabled for all users are
     * considered.
     *
     * @param featureKey key of the feature to be checked
     * @return <code>true</code> if the feature is valid and enabled, either for all users or the current user only;
     * <code>false</code> otherwise.
     * @see ValidFeatureKeyPredicate
     *
     * @deprecated use {@link #isEnabledForCurrentUser(String)} instead
     */
    @Deprecated
    boolean isFeatureEnabledForCurrentUser(String featureKey);

    /**
     * Checks if a dark feature is enabled for all users or just for the given user. In case the user is anonymous,
     * only features enabled for all users are considered.
     *
     * @param userKey    the key of the user being queried; <code>null</code> represents the anonymous user
     * @param featureKey key of the feature to be checked
     * @return <code>true</code> if the feature key is valid and enabled, either for all users or the current user only;
     * <code>false</code> otherwise.
     * @throws IllegalArgumentException if the user doesn't exist
     * @see ValidFeatureKeyPredicate
     *
     * @deprecated use {@link #isEnabledForUser(UserKey, String)} instead
     */
    @Deprecated
    boolean isFeatureEnabledForUser(@Nullable UserKey userKey, String featureKey);

    /**
     * Returns true if the current acting user has permission to change dark features for all users. This is a nothrow
     * method and should return a value instead of throw an exception.
     *
     * @return <code>true</code> iff the current acting user has permission to change dark features for all users,
     * <code>false</code> otherwise
     */
    boolean canManageFeaturesForAllUsers();

    /**
     * Enable the given dark feature all users. The acting user must have permission to change dark features for all
     * users.
     *
     * @param featureKey key of the feature to be enabled
     * @throws InvalidFeatureKeyException if the feature key is not valid
     * @throws MissingPermissionException if the user has not the required permission
     * @throws IllegalStateException      if the update failed
     * @see ValidFeatureKeyPredicate
     * @see #canManageFeaturesForAllUsers()
     */
    void enableFeatureForAllUsers(String featureKey);

    /**
     * Disable the given dark feature for all users. The acting user must have permission to change dark features for
     * all users.
     *
     * @param featureKey key of the feature to be disabled
     * @throws InvalidFeatureKeyException if the feature key is not valid
     * @throws MissingPermissionException if the user has not the required permission
     * @throws IllegalStateException      if the update failed
     * @see ValidFeatureKeyPredicate
     * @see #canManageFeaturesForAllUsers()
     */
    void disableFeatureForAllUsers(String featureKey);

    /**
     * Enable a dark feature for the current user only. Anonymous users are not supported. <strong>If the feature is
     * already enabled for all users, the user will still be able to use it.</strong>
     *
     * @param featureKey key of the feature to enable
     * @throws InvalidFeatureKeyException if the feature key is not valid
     * @throws IllegalStateException      if the current user could not be resolved, is anonymous or the update failed due to
     *                                    any other reason
     * @see ValidFeatureKeyPredicate
     */
    void enableFeatureForCurrentUser(String featureKey);

    /**
     * Enable a dark feature for the given user only. Anonymous users are not supported. <strong>If the feature is
     * already enabled for all users, the user will still be able to use it.</strong>
     *
     * @param userKey    key of the user to enable the feature for; not <code>null</code>
     * @param featureKey key of the feature to be enabled
     * @throws IllegalArgumentException   if the user does not exist or is anonymous
     * @throws InvalidFeatureKeyException if the feature key is not valid
     * @throws IllegalStateException      if the update failed
     * @see ValidFeatureKeyPredicate
     */
    void enableFeatureForUser(UserKey userKey, String featureKey);

    /**
     * Disable a dark feature for the current user only. Anonymous users are not supported. <strong>If the feature is
     * enabled for all users, the current user will still be able to use it.</strong>
     *
     * @param featureKey key of the feature to be disabled
     * @throws InvalidFeatureKeyException if the feature key is not valid
     * @throws IllegalStateException      if the current user could not be resolved, is anonymous or the update failed due to
     *                                    any other reason
     * @see ValidFeatureKeyPredicate
     */
    void disableFeatureForCurrentUser(String featureKey);

    /**
     * Disable a dark feature for the given user only. Anonymous users are not supported. <strong>If the feature is
     * enabled for all users, the user will still be able to use it.</strong>
     *
     * @param userKey    key of the user to disable the feature for; not <code>null</code>
     * @param featureKey key of the feature to be disabled
     * @throws IllegalArgumentException   if the user does not exist or is anonymous
     * @throws InvalidFeatureKeyException if the feature key is not valid
     * @throws IllegalStateException      if the update failed
     * @see ValidFeatureKeyPredicate
     */
    void disableFeatureForUser(UserKey userKey, String featureKey);

    /**
     * @return all dark features enabled for all users.
     */
    EnabledDarkFeatures getFeaturesEnabledForAllUsers();

    /**
     * Return features enabled for the current user (must be called within the context of a request). In case the
     * current user could not be resolved or is anonymous, all dark features enabled for all users are returned instead.
     *
     * @return all dark features applicable for the current user.
     */
    EnabledDarkFeatures getFeaturesEnabledForCurrentUser();

    /**
     * Return enabled features for a given user. In case the current user is anonymous, all global enabled features are
     * returned.
     *
     * @param userKey key of the user being queried; <code>null</code> represents the anonymous user
     * @return all dark features applicable for the given user
     * @throws IllegalArgumentException if the user doesn't exist
     */
    EnabledDarkFeatures getFeaturesEnabledForUser(@Nullable UserKey userKey);

}

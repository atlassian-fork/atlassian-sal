package com.atlassian.sal.api.lifecycle;

/**
 * Triggers lifecycle events on {@link LifecycleAware} components.
 * <p>
 * <strong>Implementation note:</strong> Invoking the {@link #start()} method on startup and restore satisfies the two of the
 * lifecycle requirements outlined on the {@link LifecycleAware} javadoc. The third (starting components that are enabled sometime
 * after the host application starts up) can be done using an OSGi {@code ServiceListener}.
 * <p>
 * See {@code com.atlassian.sal.core.lifecycle.DefaultLifecycleManager.LifecycleAwareServiceListener} in {@code sal-core} for an
 * example.
 *
 * @since 2.0
 */
public interface LifecycleManager {
    /**
     * Triggers {@link LifecycleAware#onStart()} on all enabled {@link LifecycleAware} components. This method can be called
     * multiple times but will only start components once and only once the plugin framework has started.
     */
    void start();

    /**
     * @return {@code true} if application is set up and ready to run, {@code false} otherwise.
     */
    boolean isApplicationSetUp();
}

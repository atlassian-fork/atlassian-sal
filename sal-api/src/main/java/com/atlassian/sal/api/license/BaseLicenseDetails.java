package com.atlassian.sal.api.license;

import com.atlassian.annotations.PublicApi;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Date;

/**
 * Common license properties that appear in all the different kind of Product Licenses that may occur in Atlassian
 * applications.
 * <p>
 * This interface must not contain properties which may have multiple values in Multi-Product Platform licenses
 * (eg NumberOfUsers).
 */
@SuppressWarnings("UnusedDeclaration")
@PublicApi
public interface BaseLicenseDetails {
    /**
     * Returns true if this is an evaluation license.
     *
     * @return true if this is an evaluation license.
     * @see #getLicenseTypeName()
     */
    boolean isEvaluationLicense();

    /**
     * Returns the license type name exactly as it appears in the license.
     *
     * @return the license type name exactly as it appears in the license.
     * @see #isEvaluationLicense()
     */
    @Nonnull
    String getLicenseTypeName();

    /**
     * Returns the Organisation Name.
     *
     * @return the Organisation Name.
     */
    String getOrganisationName();

    /**
     * Returns the Support Entitlement Number (SEN) for this license.
     * <p>
     * This should only return null for Development licenses.
     *
     * @return the Support Entitlement Number (SEN) for this license.
     */
    @Nullable
    String getSupportEntitlementNumber();

    /**
     * Returns the description that appears in the license.
     *
     * @return the description that appears in the license.
     */
    String getDescription();

    /**
     * Returns the Server-ID as it appears in this license.
     *
     * @return the Server-ID as it appears in this license.
     */
    String getServerId();

    /**
     * Returns true if this license never expires.
     * <p>
     * This is equivalent to calling:
     * <pre>    getLicenseExpiryDate() == null</pre>
     *
     * @return true if this license never expires.
     */
    boolean isPerpetualLicense();

    /**
     * Returns the license expiry date.
     * <p>
     * Perpetual licenses will never expire and will return a null expiry date.<br>
     * Subscription licenses will have an expiry date.<br>
     * It is recommended that you display the expiry date to users for subscription licenses, and the maintenance
     * expiry date to users for perpetual licenses.
     * </p>
     *
     * @return the license expiry date or null if this is a perpetual license.
     * @see #isPerpetualLicense()
     * @see #getMaintenanceExpiryDate()
     */
    @Nullable
    Date getLicenseExpiryDate();

    /**
     * Returns the Maintenance expiry date.
     * That is, the date up until which the customer can receive support.
     *
     * @return the Maintenance expiry date.
     */
    @Nullable
    Date getMaintenanceExpiryDate();

    /**
     * Returns true if this is a "Data Center" license.
     *
     * @return true if this is a "Data Center" license.
     */
    boolean isDataCenter();

    /**
     * Returns true if this is an "Enterprise Licensing Agreement" license.
     *
     * @return true if this is an "Enterprise Licensing Agreement" license.
     */
    boolean isEnterpriseLicensingAgreement();

    /**
     * Retrieves an arbitrary property from the license.
     *
     * @param key the property key
     * @return the value of the given property in the license.
     */
    @Nullable
    String getProperty(@Nonnull String key);
}

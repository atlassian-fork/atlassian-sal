package com.atlassian.sal.api.usersettings;

import com.atlassian.sal.api.user.UserKey;
import com.google.common.base.Function;

/**
 * Service for getting and updating immutable UserSettings objects stored against a user name
 *
 * UserSettings can be used to store values of type String, Boolean and Long - nothing more.
 * Max key length is {@link UserSettingsService#MAX_KEY_LENGTH}, Values of type String also have a max length of {@link UserSettingsService#MAX_STRING_VALUE_LENGTH}
 *
 * @since 2.9
 */
public interface UserSettingsService {

    String USER_SETTINGS_PREFIX = "sal_";

    int MAX_STRING_VALUE_LENGTH = 255;

    /**
     * Key length is limited by DB constraints.
     */
    int MAX_KEY_LENGTH = 200 - USER_SETTINGS_PREFIX.length();

    /**
     * Gets the UserSettings for the user with name userName.
     *
     * @param userName name of the user whose user settings are to be retrieved
     * @return a UserSettings for the user with name userName,
     * @throws IllegalArgumentException if no user could be found with that name
     * @deprecated since 2.10, use {@link #getUserSettings(com.atlassian.sal.api.user.UserKey)} instead
     */
    @Deprecated
    UserSettings getUserSettings(String userName);

    /**
     * Gets the UserSettings for the given user.
     *
     * @param userKey key of a user whose user settings are to be retrieved
     * @return a UserSettings for the user with name userName,
     * @throws IllegalArgumentException if no user could be found with that name
     */
    UserSettings getUserSettings(UserKey userKey);

    /**
     * Updates the UserSettings stored for this user with name UserName. Implementations of this interface will ensure
     * that updateFunctions provided to this method are called in a threadsafe manner.
     *
     * Consumers can throw RuntimeExceptions within updateFunction to control flow when the input to updateFunction
     * is unexpected. As such, implementers must either rethrow caught RuntimeExceptions, or not catch them in the first place.
     *
     * The intended behaviour of this function is that the return value of updateFunction be stored against the specified user.
     * However, product implementations do not do this, and update the user settings in the underlying database in response to calls
     * to the UserSettingsBuilder provided to updateFunction, and ignore the return value from updateFunction.
     *
     * @param userName       name of the user whose UserSettings are to be updated. If userName does not match a known user,
     *                       updateFunction will not be called.
     * @param updateFunction function to update a user's UserSettings. The parameter to updateFunction is a
     *                       UserSettingsBuilder whose contents match the UserSettings for the provided user.
     * @throws IllegalArgumentException      if no user could be found with that name
     * @throws UnsupportedOperationException if updateFunction creates entries with key length &gt; {@link UserSettingsService#MAX_KEY_LENGTH} or with a String value
     *                                       with length &gt; {@link UserSettingsService#MAX_STRING_VALUE_LENGTH}
     * @deprecated since 2.10, use {@link #updateUserSettings(com.atlassian.sal.api.user.UserKey, com.google.common.base.Function)} instead
     */
    @Deprecated
    void updateUserSettings(String userName, Function<UserSettingsBuilder, UserSettings> updateFunction);

    /**
     * Updates the UserSettings stored for this user. Implementations of this interface will ensure that updateFunctions
     * provided to this method are called in a threadsafe manner.
     *
     * Consumers can throw RuntimeExceptions within updateFunction to control flow when the input to updateFunction
     * is unexpected. As such, implementers must either rethrow caught RuntimeExceptions, or not catch them in the first place.
     *
     * The intended behaviour of this function is that the return value of updateFunction be stored against the specified user.
     * However, product implementations do not do this, and update the user settings in the underlying database in response to calls
     * to the UserSettingsBuilder provided to updateFunction, and ignore the return value from updateFunction.
     *
     * This function will be deprecated in a future version of SAL with replacements with a more appropriate signature, or the
     * intended behaviour, or both.
     *
     * @param userKey        key of a user whose UserSettings are to be updated. If user is null or does not exist updateFunction will not be called.
     * @param updateFunction function to update a user's UserSettings. The parameter to updateFunction is a
     *                       UserSettingsBuilder whose contents match the UserSettings for the provided user.
     * @throws IllegalArgumentException      if no user could be found with that name
     * @throws UnsupportedOperationException if updateFunction creates entries with key length &gt; {@link UserSettingsService#MAX_KEY_LENGTH} or with a String value
     *                                       with length &gt; {@link UserSettingsService#MAX_STRING_VALUE_LENGTH}
     */
    void updateUserSettings(UserKey userKey, Function<UserSettingsBuilder, UserSettings> updateFunction);
}

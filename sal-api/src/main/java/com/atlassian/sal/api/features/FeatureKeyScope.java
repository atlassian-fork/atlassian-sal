package com.atlassian.sal.api.features;

/**
 * The scope of an enabled feature key.
 *
 * @since 2.10
 */
public enum FeatureKeyScope {
    /**
     * Aka <em>system feature</em>, cannot be disabled during runtime.
     */
    ALL_USERS_READ_ONLY,

    /**
     * Aka <em>site wide feature</em>, can be disabled during runtime.
     */
    ALL_USERS,

    /**
     * The feature is enabled for the current user only.
     */
    CURRENT_USER_ONLY
}

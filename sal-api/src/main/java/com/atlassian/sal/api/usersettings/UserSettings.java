package com.atlassian.sal.api.usersettings;

import io.atlassian.fugue.Option;

import java.util.Set;

/**
 * An immutable container of user-specific settings.
 *
 * Key is global - two values of different types can _not_ be stored against a common key.
 */
public interface UserSettings {
    /**
     * @param key the setting key being queried
     * @return a {@link com.atlassian.fugue.Option.Some Some} containing the String stored against key if one exists (and is a String),
     * a {@link com.atlassian.fugue.Option#none()} otherwise.
     * @throws IllegalArgumentException if key is null or longer than {@link UserSettingsService#MAX_KEY_LENGTH} characters.
     */
    Option<String> getString(String key);

    /**
     * @param key the setting key being queried
     * @return a {@link Option.Some Some} containing the Boolean stored against key if one exists (and is a Boolean),
     * a {@link com.atlassian.fugue.Option#none()} otherwise.
     * @throws IllegalArgumentException if key is null or longer than {@link UserSettingsService#MAX_KEY_LENGTH} characters.
     */
    Option<Boolean> getBoolean(String key);

    /**
     * @param key the setting key being queried
     * @return a {@link Option.Some Some} containing the Long stored against key if one exists (and is a Long),
     * a {@link com.atlassian.fugue.Option#none()} otherwise.
     * @throws IllegalArgumentException if key is null or longer than {@link UserSettingsService#MAX_KEY_LENGTH} characters.
     */
    Option<Long> getLong(String key);

    /**
     * @return the set of keys known to this UserSettings
     */
    Set<String> getKeys();
}

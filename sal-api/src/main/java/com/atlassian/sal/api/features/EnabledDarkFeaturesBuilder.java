package com.atlassian.sal.api.features;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import javax.annotation.Nullable;
import java.util.Set;

/**
 * @since 2.10
 */
public class EnabledDarkFeaturesBuilder {
    private final ImmutableMap.Builder<FeatureKeyScope, ImmutableSet<String>> builder = ImmutableMap.builder();

    /**
     * Set features enabled for all users which cannot be changed during runtime.
     *
     * @param enabledFeatureKeys enabled features for all users; can be <code>null</code>
     * @return the builder itself
     */
    public EnabledDarkFeaturesBuilder unmodifiableFeaturesEnabledForAllUsers(@Nullable final Set<String> enabledFeatureKeys) {
        return setEnabledFeatures(FeatureKeyScope.ALL_USERS_READ_ONLY, enabledFeatureKeys);
    }

    /**
     * Set features enabled for all users which can be changed during runtime.
     *
     * @param enabledFeatureKeys enabled features for all users; can be <code>null</code>
     * @return the builder itself
     */
    public EnabledDarkFeaturesBuilder featuresEnabledForAllUsers(@Nullable final Set<String> enabledFeatureKeys) {
        return setEnabledFeatures(FeatureKeyScope.ALL_USERS, enabledFeatureKeys);
    }

    /**
     * Set features enabled for the current user only.
     *
     * @param enabledFeatureKeys enabled features for the current user only; can be <code>null</code>
     * @return the builder itself
     */
    public EnabledDarkFeaturesBuilder featuresEnabledForCurrentUser(@Nullable final Set<String> enabledFeatureKeys) {
        return setEnabledFeatures(FeatureKeyScope.CURRENT_USER_ONLY, enabledFeatureKeys);
    }

    public EnabledDarkFeatures build() {
        return new EnabledDarkFeatures(builder.build());
    }

    private EnabledDarkFeaturesBuilder setEnabledFeatures(final FeatureKeyScope featureKeyScope, @Nullable final Set<String> enabledFeatureKeys) {
        final ImmutableSet<String> nonNullCopy = enabledFeatureKeys != null ? ImmutableSet.copyOf(enabledFeatureKeys) : ImmutableSet.of();
        builder.put(featureKeyScope, nonNullCopy);
        return this;
    }
}

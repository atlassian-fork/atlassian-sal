package com.atlassian.sal.api.features;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;

import java.util.Map;

import static com.atlassian.sal.api.features.ValidFeatureKeyPredicate.checkFeatureKey;

/**
 * A parameterised plugin module condition for enabling modules in the presence of a dark feature.
 * Pass a param with parameter name "featureKey" containing the dark feature key. Example:
 * <pre>
 *  &lt;web-item key="some-key" section="some/section" weight="1"&gt;
 *      &lt;label key="menu.title"/&gt;
 *      &lt;link&gt;/some/path&lt;/link&gt;
 *      &lt;condition class="com.atlassian.sal.api.features.DarkFeatureEnabledCondition"&gt;
 *          &lt;param name="featureKey"&gt;feature.key&lt;/param&gt;
 *      &lt;/condition&gt;
 *  &lt;/web-item&gt;
 * </pre>
 * The feature key is validated using the {@link ValidFeatureKeyPredicate}.
 *
 * @see ValidFeatureKeyPredicate
 */
public class DarkFeatureEnabledCondition implements Condition {
    private static final String FEATURE_KEY_INIT_PARAMETER_NAME = "featureKey";
    private final DarkFeatureManager darkFeatureManager;
    private String featureKey;

    public DarkFeatureEnabledCondition(final DarkFeatureManager darkFeatureManager) {
        this.darkFeatureManager = darkFeatureManager;
    }

    @Override
    public void init(final Map<String, String> params) throws PluginParseException {
        if (params.containsKey(FEATURE_KEY_INIT_PARAMETER_NAME)) {
            featureKey = checkFeatureKey(params.get(FEATURE_KEY_INIT_PARAMETER_NAME));
        } else {
            throw new PluginParseException("Parameter '" + FEATURE_KEY_INIT_PARAMETER_NAME + "' is mandatory.");
        }
    }

    @Override
    public boolean shouldDisplay(final Map<String, Object> stringObjectMap) {
        try {
            return darkFeatureManager.isFeatureEnabledForCurrentUser(featureKey);
        } catch (RuntimeException e) {
            return false;
        }
    }
}

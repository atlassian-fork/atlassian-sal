package com.atlassian.sal.api.features;

import com.google.common.base.Predicate;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.regex.Pattern;

/**
 * A given string represents a valid feature key if the following conditions are satisfied:
 * <ul>
 * <li>Contains alphanumeric characters including the dot (.), dash (,) and underscore (_) only</li>
 * <li>The minimal size is at least one character, while the upper limit is not restricted</li>
 * </ul>
 *
 * @since 2.10
 */
@Immutable
public enum ValidFeatureKeyPredicate implements Predicate<String> {
    INSTANCE;

    private static final Pattern VALID_FEATURE_KEY_PATTERN = Pattern.compile("[\\w\\.\\-]+");

    /**
     * Verify that the given string represents a valid feature key.
     *
     * @param input a feature key candidate
     * @return <code>true</code> if the given input is an acceptable feature key, <code>false</code> otherwise
     * @see ValidFeatureKeyPredicate
     */
    public static boolean isValidFeatureKey(@Nullable final String input) {
        return INSTANCE.apply(input);
    }

    /**
     * Ensure that the given input string is a valid feature key. Otherwise an exception is thrown.
     *
     * @param input the expected feature key
     * @return the input if it is a valid feature key
     * @throws InvalidFeatureKeyException if the input is not a valid feature key
     */
    public static String checkFeatureKey(@Nullable final String input) {
        if (isValidFeatureKey(input)) {
            return input;
        } else {
            throw new InvalidFeatureKeyException("Invalid feature key: '" + input + "'");
        }
    }

    /**
     * Verify that the given string represents a valid feature key.
     *
     * @param input a feature key candidate
     * @return <code>true</code> if the given input is an acceptable feature key, <code>false</code> otherwise
     */
    @Override
    public boolean apply(@Nullable final String input) {
        return input != null && VALID_FEATURE_KEY_PATTERN.matcher(input).matches();
    }
}

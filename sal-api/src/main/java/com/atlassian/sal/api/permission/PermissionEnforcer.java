package com.atlassian.sal.api.permission;

import com.atlassian.annotations.PublicApi;

/**
 * Allows clients to easily verify that the caller has sufficient permission to access the resource.
 * {@code PermissionEnforcer} takes all permissions into account from the current security context. This includes the
 * calling user's permissions, but also any permission escalations or restrictions that may be in effect (e.g.
 * read-only personal access tokens).
 * <p>
 * The host application ensures that any thrown {@link AuthorisationException} is handled correctly:
 *
 * <h3>Web requests</h3>
 * <ul>
 *     <li>If the user is not authenticated, the user will be redirected to the login page</li>
 *     <li>If the user is authenticated, but lacks the required authorisation, an appropriate error page will be
 *         displayed</li>
 * </ul>
 *
 * <h3>REST requests</h3>
 * <ul>
 *     <li>If the user is not authenticated, a 401 error response is returned</li>
 *     <li>If the user is authenticated, but lacks the required authorisation, a 403 error is returned</li>
 * </ul>
 *
 * @since 3.2
 */
@PublicApi
public interface PermissionEnforcer {

    /**
     * Verifies that the current security context has sufficient authorisation to perform administration tasks. This can
     * either be because the user is an administrator, or because the security context is running with temporarily
     * elevated permissions.
     *
     * @throws AuthorisationException if the current security context lacks the required authorisation
     * @throws NotAuthenticatedException if the current security context lacks the required authorisation <em>and</em>
     *                                   the current user is not authenticated
     */
    void enforceAdmin() throws AuthorisationException;

    /**
     * Verifies that the current user is authenticated.
     *
     * @throws NotAuthenticatedException if the user is not authenticated
     */
    void enforceAuthenticated() throws NotAuthenticatedException;

    /**
     * Verifies that the current security context has sufficient authorisation to perform system administration tasks.
     * This can either be because the user is a system administrator, or because the security context is running with
     * temporarily elevated permissions.
     *
     * @throws AuthorisationException if the current security context lacks the required authorisation
     * @throws NotAuthenticatedException if the current security context lacks the required authorisation <em>and</em>
     *                                   the current user is not authenticated
     */
    void enforceSystemAdmin() throws AuthorisationException;

    /**
     * Tests whether the current security context has sufficient authorisation to perform administration tasks. This can
     * either be because the user is an administrator, or because the security context is running with temporarily
     * elevated permissions.
     *
     * @return {@code true} if the current security context has sufficient authorisation to perform administration
     *         tasks, otherwise {@code false}
     */
    boolean isAdmin();

    /**
     * @return {@code true} if the current user is authenticated
     */
    boolean isAuthenticated();

    /**
     * Tests whether the current security context has sufficient authorisation to perform system administration tasks.
     * This can either be because the user is a system administrator, or because the security context is running with
     * temporarily elevated permissions.
     *
     * @return {@code true} if the current security context has sufficient authorisation to perform system
     *         administration tasks, otherwise {@code false}
     */
    boolean isSystemAdmin();
}

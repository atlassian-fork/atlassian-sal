<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>com.atlassian.pom</groupId>
        <artifactId>public-pom</artifactId>
        <version>5.0.21</version>
    </parent>

    <groupId>com.atlassian.sal</groupId>
    <artifactId>sal-parent</artifactId>
    <version>4.2.0-SNAPSHOT</version>
    <packaging>pom</packaging>

    <name>Shared Application Access Layer POM</name>
    <licenses>
        <license>
            <name>BSD License</name>
            <url>http://opensource.org/licenses/BSD-3-Clause</url>
            <distribution>repo</distribution>
        </license>
    </licenses>

    <modules>
        <module>sal-api</module>
        <module>sal-spi</module>
        <module>sal-test-resources</module>
        <module>sal-core</module>
        <module>sal-spring</module>
        <module>sal-trust-api</module>
        <module>sal-trusted-apps-plugin-support</module>
    </modules>

    <scm>
        <connection>scm:git:git@bitbucket.org:atlassian/atlassian-sal.git</connection>
        <developerConnection>scm:git:git@bitbucket.org:atlassian/atlassian-sal.git</developerConnection>
        <url>https://bitbucket.org/atlassian/atlassian-sal</url>
        <tag>HEAD</tag>
    </scm>
    <issueManagement>
        <system>Jira</system>
        <url>https://ecosystem.atlassian.net/projects/SAL</url>
    </issueManagement>
    <ciManagement>
        <system>Bamboo</system>
        <url>https://ecosystem-bamboo.internal.atlassian.com/browse/SAL</url>
    </ciManagement>
    <distributionManagement>
        <site>
            <id>atlassian-documentation</id>
            <url>scpexe://docs-app.internal.atlassian.com/var/www/domains/docs.atlassian.com/${project.artifactId}/${project.version}</url>
        </site>
    </distributionManagement>

    <properties>
        <platform.version>5.0.0</platform.version>
        <amps.version>8.0.0</amps.version>
        <seraph.version>4.0.0</seraph.version>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <atlassian.security.version>3.2.11</atlassian.security.version>
        <asm.version>6.1.1</asm.version>
        <jaxb.version>2.3.0</jaxb.version>
        <osgi.export.version>4.0.0</osgi.export.version>
        <failOnMilestoneOrReleaseCandidateDeps>false</failOnMilestoneOrReleaseCandidateDeps>
    </properties>

    <dependencyManagement>
        <dependencies>

            <dependency>
                <groupId>com.atlassian.platform</groupId>
                <artifactId>platform</artifactId>
                <version>${platform.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <dependency>
                <groupId>com.atlassian.platform</groupId>
                <artifactId>third-party</artifactId>
                <version>${platform.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <!-- Project Dependencies -->
            <dependency>
                <groupId>com.atlassian.sal</groupId>
                <artifactId>sal-api</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>com.atlassian.sal</groupId>
                <artifactId>sal-spi</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>com.atlassian.sal</groupId>
                <artifactId>sal-core</artifactId>
                <version>${project.version}</version>
            </dependency>

            <!-- SAL Trusted Apps -->
            <dependency>
                <groupId>com.atlassian.sal</groupId>
                <artifactId>sal-trust-api</artifactId>
                <version>${project.version}</version>
            </dependency>

            <dependency>
                <groupId>com.atlassian.seraph</groupId>
                <artifactId>atlassian-seraph</artifactId>
                <version>${seraph.version}</version>
            </dependency>
            <dependency>
                <groupId>com.atlassian.security</groupId>
                <artifactId>atlassian-secure-random</artifactId>
                <version>${atlassian.security.version}</version>
            </dependency>
            <dependency>
                <groupId>com.atlassian.security</groupId>
                <artifactId>atlassian-secure-utils</artifactId>
                <version>${atlassian.security.version}</version>
            </dependency>
            <dependency>
                <groupId>javax.xml.bind</groupId>
                <artifactId>jaxb-api</artifactId>
                <version>${jaxb.version}</version>
            </dependency>
            <dependency>
                <groupId>com.sun.xml.bind</groupId>
                <artifactId>jaxb-core</artifactId>
                <version>${jaxb.version}</version>
            </dependency>
            <dependency>
                <groupId>com.sun.xml.bind</groupId>
                <artifactId>jaxb-impl</artifactId>
                <version>${jaxb.version}</version>
            </dependency>
            <dependency>
                <groupId>org.ow2.asm</groupId>
                <artifactId>asm</artifactId>
                <version>${asm.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <filtering>true</filtering>
                <includes>
                    <include>atlassian-plugin.xml</include>
                </includes>
            </resource>
            <resource>
                <directory>src/main/resources</directory>
                <filtering>false</filtering>
                <excludes>
                    <exclude>atlassian-plugin.xml</exclude>
                </excludes>
            </resource>
        </resources>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-javadoc-plugin</artifactId>
                    <!-- Overrides parent pom to get fix for [MJAVADOC-517] NPE under Java 10 RC -->
                    <version>3.0.1</version>
                    <configuration>
                        <doclint>all,-missing,-reference</doclint>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>com.atlassian.maven.plugins</groupId>
                    <artifactId>amps-maven-plugin</artifactId>
                    <version>${amps.version}</version>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>
</project>

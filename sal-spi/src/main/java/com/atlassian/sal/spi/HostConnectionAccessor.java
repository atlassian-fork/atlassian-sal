package com.atlassian.sal.spi;

import io.atlassian.fugue.Option;
import com.atlassian.sal.api.rdbms.ConnectionCallback;

import javax.annotation.Nonnull;

/**
 * Interface allowing SAL to execute a callback with a {@link java.sql.Connection}, in the context of a transaction.
 *
 * Host must export this.
 *
 * Host must implement this; may use {@code com.atlassian.sal.spring.connection.SpringHostConnectionAccessor}
 *
 * @since 3.0
 */
public interface HostConnectionAccessor {
    /**
     * Execute a callback which is supplied a {@link java.sql.Connection}, within a transaction.
     *
     * It is up to the caller as to whether it attempts to participate in an existing transaction. This is explicitly
     * explained in {@link com.atlassian.sal.api.rdbms.TransactionalExecutor}.
     *
     * In the case of the callback returning normally:
     * <ul>
     *   <li>If it is a new connection/transaction that the implementation started then the transaction must be committed, or</li>
     *   <li>In the case of an existing transaction, the implementation will do nothing and wait for the larger transaction to be committed</li>
     * </ul>
     * In the case of the callback throwing a RuntimeException:
     * <ul>
     *   <li>The implementation will mark the transaction as "rollback only" and rethrow the error</li>
     * </ul>
     *
     * The host must ensure that the Connection provided to the ConnectionCallback has <code>autoCommit=false</code>.
     *
     * The following methods are guaranteed not to be called on <code>connection</code>:
     * {@link java.sql.Connection#setAutoCommit(boolean)}
     * {@link java.sql.Connection#commit()}
     * {@link java.sql.Connection#close()}
     * {@link java.sql.Connection#rollback()}
     * {@link java.sql.Connection#setReadOnly(boolean)}
     * {@link java.sql.Connection#abort(java.util.concurrent.Executor)}
     * {@link java.sql.Connection#setCatalog(String)}
     * {@link java.sql.Connection#setSchema(String)}
     * {@link java.sql.Connection#setTransactionIsolation(int)}
     * {@link java.sql.Connection#setNetworkTimeout(java.util.concurrent.Executor, int)}
     *
     * @param readOnly       <code>connection</code> should be read only
     * @param newTransaction attempt to create a new transaction even if there is already a "current" transaction
     * @param callback       mandatory
     * @return return value of <code>callback</code>
     */
    <A> A execute(final boolean readOnly, final boolean newTransaction, @Nonnull ConnectionCallback<A> callback);

    /**
     * Returns the configured schema name (if any), for connections provided during {@link #execute(boolean, boolean, ConnectionCallback)}
     *
     * @return schema name, if there is one
     */
    @Nonnull
    Option<String> getSchemaName();
}

package com.atlassian.sal.core.lifecycle;

import com.atlassian.sal.api.lifecycle.LifecycleAware;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

public class LegacyLifecycleAwareGenerator implements Opcodes {

    private static byte[] generateByteCode() {
        final ClassWriter cw = new ClassWriter(0);

        cw.visit(V1_6,
                ACC_PUBLIC + ACC_SUPER,
                "com/atlassian/sal/core/lifecycle/LegacyLifecycleAware",
                null,
                "java/lang/Object",
                new String[]{"com/atlassian/sal/api/lifecycle/LifecycleAware"});

        final MethodVisitor ctor = cw.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
        ctor.visitCode();
        ctor.visitVarInsn(ALOAD, 0);
        ctor.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
        ctor.visitInsn(RETURN);
        ctor.visitMaxs(1, 1);
        ctor.visitEnd();

        final MethodVisitor onStart = cw.visitMethod(ACC_PUBLIC, "onStart", "()V", null, null);
        onStart.visitCode();
        onStart.visitInsn(RETURN);
        onStart.visitMaxs(0, 1);
        onStart.visitEnd();

        cw.visitEnd();

        return cw.toByteArray();
    }

    private static class DynamicClassLoader extends ClassLoader {
        public Class<?> define(String className, byte[] bytecode) {
            return super.defineClass(className, bytecode, 0, bytecode.length);
        }
    }

    @SuppressWarnings("unchecked")
    public static LifecycleAware newInstance() throws Exception {
        final DynamicClassLoader loader = new DynamicClassLoader();
        return (LifecycleAware) loader.define("com.atlassian.sal.core.lifecycle.LegacyLifecycleAware", generateByteCode())
                .newInstance();
    }
}

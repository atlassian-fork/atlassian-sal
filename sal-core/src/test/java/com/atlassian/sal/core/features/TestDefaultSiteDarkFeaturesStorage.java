package com.atlassian.sal.core.features;

import com.atlassian.sal.api.features.SiteDarkFeaturesStorage;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.hamcrest.MockitoHamcrest.argThat;

@RunWith(MockitoJUnitRunner.class)
public class TestDefaultSiteDarkFeaturesStorage {
    private static final String FEATURE_KEY = "dark.feature";
    private static final String ANOTHER_DARK_FEATURE = "another.dark.feature";

    @Mock
    private PluginSettingsFactory pluginSettingsFactory;
    @Mock
    private PluginSettings pluginSettings;

    @Before
    public void setUp() {
        when(pluginSettingsFactory.createGlobalSettings()).thenReturn(pluginSettings);
    }

    @Test
    public void enableDarkFeature() {
        givenEnabledDarkFeatures();
        createStorage().enable(FEATURE_KEY);
        thenPluginSettingsUpdated();
    }

    @Test
    public void ignoreEnableForAlreadyEnabledDarkFeature() {
        givenEnabledDarkFeatures(FEATURE_KEY);
        createStorage().enable(FEATURE_KEY);
        thenPluginSettingsNotUpdated();
    }

    @Test
    public void ignoreDisableForNotEnabledDarkFeature() {
        givenEnabledDarkFeatures();
        createStorage().disable(FEATURE_KEY);
        thenPluginSettingsNotUpdated();
    }

    @Test
    public void disableDarkFeature() {
        givenEnabledDarkFeatures(FEATURE_KEY);
        createStorage().disable(FEATURE_KEY);
        thenPluginSettingsUpdated();
    }

    @Test
    public void noStoredFeatures() {
        when(pluginSettings.get(anyString())).thenReturn(null);
        final ImmutableSet<String> enabledDarkFeatures = createStorage().getEnabledDarkFeatures();
        assertTrue(enabledDarkFeatures.isEmpty());
    }

    @Test
    public void getEmptySetOfEnabledDarkFeatures() {
        givenEnabledDarkFeatures();
        final ImmutableSet<String> enabledDarkFeatures = createStorage().getEnabledDarkFeatures();
        assertTrue(enabledDarkFeatures.isEmpty());
    }

    @Test
    public void getOneEnabledDarkFeature() {
        givenEnabledDarkFeatures(FEATURE_KEY);
        final ImmutableSet<String> enabledDarkFeatures = createStorage().getEnabledDarkFeatures();
        assertThat(enabledDarkFeatures, hasItem(FEATURE_KEY));
    }

    @Test
    public void getTwoEnabledDarkFeatures() {
        givenEnabledDarkFeatures(FEATURE_KEY, ANOTHER_DARK_FEATURE);
        final ImmutableSet<String> enabledDarkFeatures = createStorage().getEnabledDarkFeatures();
        assertThat(enabledDarkFeatures, hasItem(FEATURE_KEY));
        assertThat(enabledDarkFeatures, hasItem(ANOTHER_DARK_FEATURE));
    }

    private SiteDarkFeaturesStorage createStorage() {
        return new DefaultSiteDarkFeaturesStorage(pluginSettingsFactory);
    }

    private void givenEnabledDarkFeatures(final String... featureKeys) {
        when(pluginSettings.get(anyString())).thenReturn(Arrays.asList(featureKeys));
    }

    private void thenPluginSettingsUpdated() {
        verify(pluginSettings).put(anyString(), argThat(instanceOf(List.class)));
    }

    private void thenPluginSettingsNotUpdated() {
        verify(pluginSettings, never()).put(anyString(), any(Object.class));
    }
}

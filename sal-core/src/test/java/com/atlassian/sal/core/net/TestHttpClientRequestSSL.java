package com.atlassian.sal.core.net;

import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseHandler;
import com.atlassian.sal.api.net.ResponseProtocolException;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import org.apache.commons.io.IOUtils;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.NetworkConnector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.util.Set;

import static org.apache.commons.io.IOUtils.write;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.junit.Assume.assumeFalse;

public class TestHttpClientRequestSSL {
    private HttpTestServer server;
    private HttpClientRequestFactory requestFactory;

    private SSLContext originalSSLContext;

    @Before
    public void setUp() throws NoSuchAlgorithmException, KeyManagementException {
        originalSSLContext = SSLContext.getDefault();
        SSLContext.setDefault(getGullibleSSLContext());
        requestFactory = new HttpClientRequestFactory();
    }

    @After
    public void stopServer() throws Exception {
        SSLContext.setDefault(originalSSLContext);
        System.clearProperty(SystemPropertiesConnectionConfig.HTTP_MAX_REDIRECTS_PROPERTY_NAME);
        if (server != null) {
            server.stop();
        }
    }

    @Test
    public void testHttpClientAccessHttpUrl() throws Exception {
        server = new HttpTestServer();
        server.start();

        testGetUrl("http://localhost:" + server.getHttpPort());
    }

    @Test
    public void testHttpClientAccessHttpsWithSSL() throws Exception {
        server = new SslTestServer();
        server.start();

        testGetUrl("https://localhost:" + server.getHttpPort());
    }

    @Test
    public void testHttpClientAccessHttpsWithSSLv3Only() throws Exception {
        assumeFalse(isSSLv3Disabled());

        server = new SslTestServer("SSLv3");
        server.start();
        try {
            System.setProperty("https.protocols", "SSLv3");
            testGetUrl("https://localhost:" + server.getHttpPort());
        } finally {
            System.clearProperty("https.protocols");
        }
    }

    @Test
    public void testHttpClientAccessHttpsWithTLSv1Only() throws Exception {
        server = new SslTestServer("TLSv1");
        server.start();

        testGetUrl("https://localhost:" + server.getHttpPort());
    }

    @Test(expected = ResponseProtocolException.class)
    public void testHttpClientAccessFailsWithSSlv3OnTLSv1() throws Exception {
        server = new SslTestServer("TLSv1");
        server.start();

        try {
            System.setProperty("https.protocols", "SSLv3");
            HttpClientRequest request = requestFactory.createRequest(Request.MethodType.GET, ("http://localhost:" + server.getHttpPort()));
            request.execute(checkResponseHandler);
        } finally {
            System.clearProperty("https.protocols");
        }
    }

    @Test(expected = ResponseProtocolException.class)
    public void testHttpClientAccessFailsWithTLSv1OnSSLv3() throws Exception {
        assumeFalse(isSSLv3Disabled());
        server = new SslTestServer("SSLv3");
        server.start();

        try {
            System.setProperty("https.protocols", "TLSv1");
            HttpClientRequest request = requestFactory.createRequest(Request.MethodType.GET, ("http://localhost:" + server.getHttpPort()));
            request.execute(checkResponseHandler);
        } finally {
            System.clearProperty("https.protocols");
        }
    }

    @Test
    public void testReusingRequestWithAndWithoutSSL() throws Exception {
        server = new HttpTestServer();
        server.start();

        HttpClientRequest request = requestFactory.createRequest(Request.MethodType.GET, ("http://localhost:" + server.getHttpPort()));
        request.execute(checkResponseHandler);

        server.stop();
        server = new SslTestServer("TLSv1");
        server.start();

        request.setUrl("https://localhost:" + server.getHttpPort());
        request.execute(checkResponseHandler);
    }

    @Test(expected = ResponseException.class)
    public void testRedirectToSSLv3() throws Exception {
        HttpTestServer targetServer = new SslTestServer("SSLv3");
        server = new HttpRedirectTestServer(targetServer, "localhost");
        server.start();

        try {
            targetServer.start();
            testGetUrl("http://localhost:" + server.getHttpPort());
        } finally {
            targetServer.stop();
        }
    }

    @Test
    public void testRedirectFromSSLv3() throws Exception {
        assumeFalse(isSSLv3Disabled());

        HttpTestServer targetServer = new HttpTestServer();
        server = new SslTestServer("SSLv3").handler(new RedirectMockHandler(targetServer, "localhost"));
        server.start();

        try {
            System.setProperty("https.protocols", "SSLv3");
            targetServer.start();
            testGetUrl("https://localhost:" + server.getHttpPort());
        } finally {
            System.clearProperty("https.protocols");
            targetServer.stop();
        }
    }

    @Test(expected = ResponseException.class)
    public void testRedirectFromTLSv1ToSSLv3FailsWithDefaultProtocolSettings() throws Exception {
        HttpTestServer targetServer = new SslTestServer("SSLv3");

        server = new SslTestServer("TLSv1").handler(new RedirectMockHandler(targetServer, "localhost"));
        server.start();
        try {
            targetServer.start();

            testGetUrl("https://localhost:" + server.getHttpPort());
        } finally {
            targetServer.stop();
        }
    }

    private ResponseHandler<HttpClientResponse> checkResponseHandler = new ResponseHandler<HttpClientResponse>() {
        @Override
        public void handle(HttpClientResponse response) throws ResponseException {
            assertEquals(200, response.getStatusCode());
            try {
                assertEquals(HttpTestServer.RESPONSE, IOUtils.toString(response.getResponseBodyAsStream(), "UTF-8"));
            } catch (IOException e) {
                fail("Failed to read response body: " + e.getMessage());
            }
        }
    };

    private void testGetUrl(String url) throws Exception {
        requestFactory.createRequest(Request.MethodType.GET, url).execute(checkResponseHandler);
    }

    private SSLContext getGullibleSSLContext() throws NoSuchAlgorithmException, KeyManagementException {
        SSLContext sc = SSLContext.getInstance("TLS");
        sc.init(null, new TrustManager[]{getGullibleTrustManager()}, null);
        return sc;
    }

    private TrustManager getGullibleTrustManager() {
        return
                new X509TrustManager() {
                    public X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
                    }

                    public void checkServerTrusted(X509Certificate[] certs, String authType) {
                    }
                };
    }

    /*
        Oracle disabled SSLv3 in Java 7_75 and 8_31
        See https://blogs.oracle.com/security/entry/january_2015_critical_patch_update
    */
    private boolean isSSLv3Disabled() {
        final String disabledAlgorithms = Security.getProperty("jdk.tls.disabledAlgorithms");
        return disabledAlgorithms != null && disabledAlgorithms.contains("SSLv3");
    }

    private class HttpTestServer {
        public static final int HTTP_PORT = 0;
        public static final String RESPONSE = "Response body";

        private Server server;
        private String responseBody;
        private Handler handler;

        public void start() throws Exception {
            configureServer();
            startServer();
        }

        private void startServer() throws Exception {
            server.start();
        }

        protected void configureServer() {
            server = new Server(HTTP_PORT);
            server.setHandler(getHandler());
        }

        public int getHttpPort() {
            return ((NetworkConnector) server.getConnectors()[0]).getLocalPort();
        }

        public Handler getHandler() {
            if (handler != null) {
                return handler;
            }

            return new AbstractHandler() {
                @Override
                public void handle(final String s, final org.eclipse.jetty.server.Request request, final HttpServletRequest httpServletRequest, final HttpServletResponse response)
                        throws IOException, ServletException {
                    setResponseBody(RESPONSE);
                    response.setStatus(200);
                    response.setContentType("text/plain;charset=utf-8");
                    write(getResponseBody(), response.getOutputStream());
                    request.setHandled(true);
                }
            };
        }

        public void stop() throws Exception {
            server.stop();
        }

        public void setResponseBody(String responseBody) {
            this.responseBody = responseBody;
        }

        public String getResponseBody() {
            return responseBody;
        }

        protected Server getServer() {
            return server;
        }

        public HttpTestServer handler(Handler handler) {
            this.handler = handler;
            return this;
        }
    }

    private class RedirectingHttpServer extends HttpTestServer {
        private final int maxRedirects;

        public RedirectingHttpServer(int maxRedirects) {
            this.maxRedirects = maxRedirects;
        }

        public Handler getHandler() {
            return new RedirectMockHandler(this, "localhost", maxRedirects);
        }
    }

    public class SslTestServer extends HttpTestServer {
        private static final int HTTPS_PORT = 0;

        private final String[] excludedProtocols;
        private final String[] includeProtocols;
        private final Set<String> protocols = Sets.newHashSet("TLSv1", "SSLv3", "SSLv2Hello");

        private ServerConnector secureConnector;

        public SslTestServer() {
            this("TLSv1,SSLv3,SSLv2Hello");
        }

        public SslTestServer(String include) {
            includeProtocols = include.split(",");
            excludedProtocols = Sets.difference(protocols, ImmutableSet.copyOf(includeProtocols))
                    .toArray(new String[0]);
        }

        @Override
        protected void configureServer() {
            super.configureServer();
            secureConnector = createSecureConnector();
            getServer().addConnector(secureConnector);
        }

        private ServerConnector createSecureConnector() {
            URL keystoreUrl = getClass().getClassLoader().getResource("keystore");
            SslContextFactory sslContextFactory = new SslContextFactory();
            sslContextFactory.setKeyStorePath(keystoreUrl.getFile());
            sslContextFactory.setKeyStorePassword("changeit");
            sslContextFactory.setExcludeCipherSuites("MD5", "SHA", "SHA1");

            sslContextFactory.setExcludeProtocols(excludedProtocols);
            sslContextFactory.setIncludeProtocols(includeProtocols);

            ServerConnector connector = new ServerConnector(getServer(), sslContextFactory);
            connector.setPort(HTTPS_PORT);

            return connector;
        }

        @Override
        public int getHttpPort() {
            return secureConnector.getLocalPort();
        }

        public Handler getHandler() {
            final Handler handler = super.getHandler();
            return new AbstractHandler() {
                @Override
                public void handle(final String s, final org.eclipse.jetty.server.Request request, final HttpServletRequest httpServletRequest, final HttpServletResponse response)
                        throws IOException, ServletException {
                    assertEquals("localhost:" + getHttpPort(), httpServletRequest.getHeader("Host"));
                    handler.handle(s, request, httpServletRequest, response);
                }
            };
        }
    }

    private class HttpRedirectTestServer extends HttpTestServer {
        private HttpRedirectTestServer(HttpTestServer targetServer, String targetHostname) throws UnknownHostException {
            handler(new RedirectMockHandler(targetServer, targetHostname));
        }
    }

    private static class RedirectMockHandler extends AbstractHandler {
        private final String targetHostname;
        private final HttpTestServer targetServer;
        private final int maxRedirects;
        private int redirectsCount = 0;

        private RedirectMockHandler(HttpTestServer targetServer, String targetHostname) throws UnknownHostException {
            this(targetServer, targetHostname, 1);
        }

        private RedirectMockHandler(HttpTestServer targetServer, String targetHostname, int maxRedirects) {
            this.targetServer = targetServer;
            this.targetHostname = targetHostname;
            this.maxRedirects = maxRedirects;
        }

        @Override
        public void handle(final String target, final org.eclipse.jetty.server.Request baseRequest, final HttpServletRequest request, final HttpServletResponse response)
                throws IOException {
            if (redirectsCount >= maxRedirects) {
                response.setStatus(200);
                write(HttpTestServer.RESPONSE, response.getOutputStream());
            } else {
                redirectsCount++;
                response.setStatus(301);
                String scheme = (targetServer instanceof SslTestServer) ? "https://" : "http://";
                final String location = scheme + targetHostname + ":" + targetServer.getHttpPort() + "?" + redirectsCount;
                response.setHeader("Location", location);
                write("Redirect", response.getOutputStream());
            }

            response.setContentType("text/plain;charset=utf-8");
            baseRequest.setHandled(true);
        }
    }
}

package com.atlassian.sal.api.features;

import com.atlassian.plugin.PluginParseException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.annotation.Nullable;
import java.util.Collections;
import java.util.Map;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestDarkFeatureEnabledCondition {
    private static final Map<String, Object> EMPTY_CONTEXT = Collections.emptyMap();
    private static final String FEATURE_KEY = "feature.key";

    @Mock
    private DarkFeatureManager darkFeatureManager;

    @Test
    public void featureEnabled() {
        when(darkFeatureManager.isFeatureEnabledForCurrentUser(FEATURE_KEY)).thenReturn(Boolean.TRUE);
        assertTrue(createAndInitWithFeatureKey(FEATURE_KEY).shouldDisplay(EMPTY_CONTEXT));
    }

    @Test
    public void featureDisabled() {
        when(darkFeatureManager.isFeatureEnabledForCurrentUser(FEATURE_KEY)).thenReturn(Boolean.FALSE);
        assertFalse(createAndInitWithFeatureKey(FEATURE_KEY).shouldDisplay(EMPTY_CONTEXT));
    }

    @Test
    public void exceptionThrownByFeatureManagerGracefullyHandled() {
        when(darkFeatureManager.isFeatureEnabledForCurrentUser(anyString())).thenThrow(new RuntimeException());
        assertFalse(createAndInitWithFeatureKey(FEATURE_KEY).shouldDisplay(EMPTY_CONTEXT));
    }

    @Test
    public void nullContextIgnored() {
        assertFalse(createAndInitWithFeatureKey(FEATURE_KEY).shouldDisplay(null));
    }

    @Test(expected = InvalidFeatureKeyException.class)
    public void invalidFeatureKeyCausesException() {
        createAndInitWithFeatureKey("   invalid   ");
    }

    @Test(expected = InvalidFeatureKeyException.class)
    public void nullFeatureKeyCausesException() {
        createAndInitWithFeatureKey(null);
    }

    @Test(expected = InvalidFeatureKeyException.class)
    public void emptyFeatureKeyCausesException() {
        createAndInitWithFeatureKey("");
    }

    @Test(expected = PluginParseException.class)
    public void missingFeatureKeyCausesException() {
        createCondition().init(Collections.<String, String>emptyMap());
    }

    private DarkFeatureEnabledCondition createAndInitWithFeatureKey(@Nullable final String featureKey) {
        final DarkFeatureEnabledCondition condition = createCondition();
        condition.init(Collections.singletonMap("featureKey", featureKey));
        return condition;
    }

    private DarkFeatureEnabledCondition createCondition() {
        return new DarkFeatureEnabledCondition(darkFeatureManager);
    }
}

package com.atlassian.sal.core.net;

public interface ConnectionConfig {
    int getSocketTimeout();

    int getConnectionTimeout();

    int getMaxRedirects();
}

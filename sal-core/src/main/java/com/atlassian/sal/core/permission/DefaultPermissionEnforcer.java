package com.atlassian.sal.core.permission;

import com.atlassian.sal.api.permission.AuthorisationException;
import com.atlassian.sal.api.permission.NotAuthenticatedException;
import com.atlassian.sal.api.permission.PermissionEnforcer;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;

public class DefaultPermissionEnforcer implements PermissionEnforcer {

    private final UserManager userManager;

    public DefaultPermissionEnforcer(UserManager userManager) {
        this.userManager = userManager;
    }

    @Override
    public void enforceAdmin() throws AuthorisationException {
        if (!userManager.isAdmin(getRemoteUserOrThrow())) {
            throw new AuthorisationException("You must be an administrator to access this resource");
        }
    }

    @Override
    public void enforceAuthenticated() throws AuthorisationException {
        getRemoteUserOrThrow();
    }

    @Override
    public void enforceSystemAdmin() throws AuthorisationException {
        if (!userManager.isSystemAdmin(getRemoteUserOrThrow())) {
            throw new AuthorisationException("You must be an administrator to access this resource");
        }
    }

    @Override
    public boolean isAdmin() {
        UserKey key = userManager.getRemoteUserKey();
        return key != null && userManager.isAdmin(key);
    }

    @Override
    public boolean isAuthenticated() {
        return userManager.getRemoteUserKey() != null;
    }

    @Override
    public boolean isSystemAdmin() {
        UserKey key = userManager.getRemoteUserKey();
        return key != null && userManager.isSystemAdmin(key);
    }

    private UserKey getRemoteUserOrThrow() {
        UserKey key = userManager.getRemoteUserKey();
        if (key == null) {
            throw new NotAuthenticatedException();
        }
        return key;
    }
}

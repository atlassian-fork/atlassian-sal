package com.atlassian.sal.core.search.query;

import com.atlassian.sal.api.search.parameter.SearchParameter;
import com.atlassian.sal.api.search.query.SearchQuery;
import com.atlassian.sal.core.search.parameter.BasicSearchParameter;
import org.apache.commons.lang3.StringUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 */
public class DefaultSearchQuery implements SearchQuery {
    private StringBuilder searchString = new StringBuilder();
    private Map<String, SearchParameter> parameters = new LinkedHashMap<>();

    public DefaultSearchQuery(String query) {
        append(query);
    }

    public SearchQuery setParameter(String name, String value) {
        parameters.put(name, new BasicSearchParameter(name, value));
        return this;
    }

    public String getParameter(String name) {
        final SearchParameter value = parameters.get(name);

        return value == null ? null : value.getValue();
    }

    public String buildQueryString() {
        StringBuilder builder = new StringBuilder(searchString);
        for (SearchParameter parameter : parameters.values()) {
            builder.append('&');
            builder.append(parameter.buildQueryString());
        }
        return builder.toString();
    }

    public SearchQuery append(String query) {
        if (StringUtils.isEmpty(query)) {
            throw new IllegalArgumentException("Cannot parse empty query string!");
        }
        if (!query.contains(SearchQuery.PARAMETER_SEPARATOR)) {
            //looks like there's no params.
            searchString.append(query);
            return this;
        }

        final String[] strings = query.split(SearchQuery.PARAMETER_SEPARATOR);
        searchString.append(strings[0]);
        for (int i = 1; i < strings.length; i++) {
            String string = strings[i];
            BasicSearchParameter searchParam = new BasicSearchParameter(string);
            parameters.put(searchParam.getName(), searchParam);
        }
        return this;
    }

    public String getSearchString() {
        try {
            return URLDecoder.decode(searchString.toString(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public int getParameter(String name, int defaultValue) {
        try {
            return Integer.parseInt(getParameter(name));
        } catch (NumberFormatException e) {
            // ignore
        }
        return defaultValue;
    }

}

package com.atlassian.sal.core.lifecycle;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.Iterables;
import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceReference;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;

import static com.atlassian.plugin.osgi.util.OsgiHeaderUtil.getPluginKey;

/**
 * A collection of methods to make lifecycle logging easier
 */
class LifecycleLog {
    @Nonnull
    static String getPluginKeyFromBundle(@Nullable final Bundle bundle) {
        return (bundle == null) ? "<stale service reference>" : getPluginKey(bundle);
    }

    @Nonnull
    static <T> String listPluginKeys(@Nonnull Collection<ServiceReference<T>> services) {
        @SuppressWarnings("unchecked")
        Iterable<String> pluginKeys = Iterables.transform(services, new Function<ServiceReference<T>, String>() {
            @Nullable
            @Override
            public String apply(@Nonnull final ServiceReference<T> service) {

                return getPluginKeyFromBundle(service.getBundle());
            }
        });

        return "[" + Joiner.on(", ").join(pluginKeys) + "]";
    }
}

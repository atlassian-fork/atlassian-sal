package com.atlassian.sal.core.executor;

import com.atlassian.sal.api.executor.ThreadLocalDelegateExecutorFactory;

import java.util.concurrent.Executor;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Executor that wraps executing runnables in a wrapper that transfers the threadlocal context
 */
class ThreadLocalDelegateExecutor implements Executor {
    private final Executor delegate;
    private final ThreadLocalDelegateExecutorFactory delegateExecutorFactory;

    ThreadLocalDelegateExecutor(Executor delegate, ThreadLocalDelegateExecutorFactory delegateExecutorFactory) {
        this.delegateExecutorFactory = checkNotNull(delegateExecutorFactory);
        this.delegate = checkNotNull(delegate);
    }

    @Override
    public void execute(Runnable runnable) {
        final Runnable wrapper = delegateExecutorFactory.createRunnable(runnable);
        delegate.execute(wrapper);
    }
}

# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [4.1.0] - Unreleased

### Added
- [SAL-378] Introduce getLocalHomeDirectory and getSharedHomeDirectory methods to ApplicationProperties.

## [4.0.0] - Released

### Changed
- [SAL-373] Now compatible with Java 9, 10 and 11
- [SAL-373] Now depends on `commons-lang3` instead of `commons-lang`
- [SAL-373] Now requires Platform 5

[SAL-373]: https://ecosystem.atlassian.net/browse/SAL-373


